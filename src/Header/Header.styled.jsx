import { styled } from '@mui/material/styles'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'

export const WSAppBar = styled(AppBar)({
  display: 'flex',
  justifyContent: 'space-between',
  flexGrow: 1,
  '@media (min-width: 768px)': {
  }
})

export const WSToolbar = styled(Toolbar)({
  display: 'flex',
  justifyContent: 'space-between',
  flexGrow: 1,

  '@media (min-width: 768px)': {
    // paddingLeft: '2%'
  }
})
