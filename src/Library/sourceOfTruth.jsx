import { baseGameGoals, europeanExpansionGoals, oceaniaExpansionGoals } from './goalArrays'

export const INIT_GOALS = baseGameGoals
export const EUROPEAN_GOALS = europeanExpansionGoals
export const OCEANIA_GOALS = oceaniaExpansionGoals
export const SIZE_OF_HANDS = 4
