<h1>Wingspan End of Round Goals Generator</h1>
<h3>What is this?</h3>
<p>
The Wingspan Goal Generator is an efficient method to randomly generate your four End of Round Goals for the board game Wingspan.
</p>

<h3>
Why did you make this?
</h3>
<p>
  My inspritation sprang from my love of
  creating tools to improve one's quality of life using code,
  and indentifing an opportunity to apply that to my love of Wingspan.
</p>
<p>
  The problem this tool attempts to solve is the lack of ease and consistency in drawing
  End of Round Goals with the physical Goal Tiles.
</p>
<p>
  I was not satisfied with shuffling the Goal Tiles
  in my hands and letting them fall out from an
  opening in my hands. This often caused multiple
  Goal Tiles to fall out and/or fall onto the ground.
</p>
<p>
  I identified that the Goal Tiles were an array of objects that I could recreate digitally and then utilize these arrays of Goal objects with a function based on the Fisher–Yates shuffle algorithm, a "coin flip" function, and array methods and mapping to more efficiently draw goals and to improve my coding skills.
</p>

<h3>How do I draw goals?</h3>
<p>First, decide which goals to include in your game. The standard game's goals are enabled by default</p>
<p>The European and/or Oceania goals can be included by enabling their respective Switch found in Goal Generator.</p>
<p>Then press Get Goals Button to randomly select four End of Round Goals to be drawn in the Draw Area.</p>
<p>Multiple games can be played without repeated goals by pressing Get Goals Button</p>
<p>I've taken care to make sure you won't draw both the back and front of the same Goal Tile</p>
<p>Finally, when you have run out of goals or would like to reshuffle all the goals press Reset Goals Button to
  the reset the generator to its default state.
</p>
<h3>Technologies used:  <a href="https://reactjs.org/">React</a>, <a href="https://mui.com/">MUI</a>, <a href="https://lodash.com/">Lodash</a>, and <a href="https://firebase.google.com/">Google Firebase</a> </h3>

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
